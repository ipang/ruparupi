<html>
  <head>
    <title><?php echo $title;?></title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.min.js');?>"></script>
  </head>
  <body>


    <form role="form" action="<?php echo base_url('tutorial/add_process');?>" method="post">
      <div class="form-group">
          <?php if(validation_errors()==TRUE) {;?>
          <div class="alert alert-danger col-xs-5">
            <?php echo validation_errors();?>
          </div><br><br><br><br><br><br><br><br><br>
          <?php };?>

        <div class="col-xs-3">
          <label for="nama"> Nama </label>
          <input type="text" id="nama" name="nama" value="" width="50%" class="form-control" value="<?php echo set_value('nama'); ?>" required="required" placeholder="Nama lengkap"/>

          <label for="username">Username</label>
          <input type="text" aria-describedby="usernamedesc" id="username" name="username"  value="<?php echo set_value('username'); ?>" value="" class="form-control" required="required" placeholder="Username"/>
          <small id="usernamedesc" class="form-text text-muted">
            Username harus terdiri dari 4-10 karakter, tidak boleh mengandung simbol dan spasi
          </small>
          <br>
          <label for="password">Password</label>
          <input type="password" id="password" name="password" required="require" class="form-control" aria-describedby="passworddesc"/>
          <small id="passworddesc" class="form-text text-muted">
              Password harus terdiri dari 7-20 karakter, mengandung angka, huruf, simbol dan tidak mengandung spasi
          </small>

          <label for="passconf">Password Confirmation</label>
          <input type="password" id="passconf" name="passconf" required="require" class="form-control" />
          <br>
          <input type="submit" class="btn btn-primary" value="Submit" name="submit"/> <input type="reset" class="btn btn-primary"/>
        </div>
      </div>
    </form>
  </body>
</html>
