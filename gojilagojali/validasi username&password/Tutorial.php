<?php
defined ('BASEPATH') or die ('No direct script access allowed');

class Tutorial extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
  }
  public function index()
  {
    $data['title']="Username & Password Validation ";
    $this->load->view('tutorial/add_user', $data);
  }
  public function add_process()
  {
    if($this->input->post('submit')) //if submit button clicked
    {
      $this->form_validation->set_rules('nama', 'Nama', 'required|min_length[3]');
      //using callback method to apply our own validation
      $this->form_validation->set_rules('username', 'Username', 'required|min_length[4]|max_length[10]|callback_username_check');
      $this->form_validation->set_rules('password', 'Password', 'required|min_length[7]|max_length[20]|callback_password_check');
      $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

      if($this->form_validation->run()==TRUE) //if input pass the validation
      {
        //what you do after validation success
      }
      else
      {
        $data['title']="Username & Password Validation ";
        $this->load->view('tutorial/add_user', $data);
      }

    }
  } //end of add_process

  //username_check validation
  public function username_check($str)
  {
    //first we check if input contain character, second we check if input contain white space.
    //for better validation info, you can make that separately
    if(!preg_match('#\W#', $str) && !preg_match('/\s/', $str) ) {
      return TRUE;
    }
    $this->form_validation->set_message('username_check', 'The {field} cannot contain symbol & white space');
    return FALSE;
  }
  //password_check validation
  public function password_check($str)
  {
    //first we check if contain contain number, alphabet, uppercase alphabet & symbol. And not contain white space
    //for better validation info, you can make that separately
     if (preg_match('#[0-9]#', $str) && preg_match('#[a-z]#', $str) && preg_match('#[A-Z]#', $str) && preg_match('#\W#', $str) && !preg_match("/\s/", $str )  ) {
       return TRUE;
     }
     $this->form_validation->set_message('password_check', 'The {field} must be more complex & cannot contain white space');
     return FALSE;
  }

} //end of tutorial class



//end of tutorial controller
 ?>
