#!/bin/bash
sudo sed -i 's/X-GIO-NoFuse=true/#X-GIO-NoFuse=true/' /usr/share/applications/libreoffice*
sudo sed -i 's/X-GIO-NoFuse=true/#X-GIO-NoFuse=true/' /usr/share/app-install/desktop/libreoffice*
# for libreoffice 5.0.0.5
sudo sed -i 's/X-GIO-NoFuse=true/#X-GIO-NoFuse=true/' /usr/local/share/applications/libreoffice*
