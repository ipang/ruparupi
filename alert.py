#!/usr/bin/env python3

from email.mime.text import MIMEText
from email.header import Header
from email.utils import formatdate

print(u'requires 3 parameters (recipient, subject, body) \t$ zabbix-gmail.sh recipient subject body')

"""
Zabbix SMTP Alert script for bitmessage.
"""

import sys
import smtplib

# Mail Account
MAIL_ACCOUNT = 'youremailaccount@bitmessage.ch'
MAIL_PASSWORD = 'yourpassword'

# Sender Name
SENDER_NAME = u'Zabbix Alert'

# Mail Server
SMTP_SERVER = 'mail.bitmessage.ch'
SMTP_PORT = 587
# TLS
SMTP_TLS = True

def send_mail(recipient, subject, body, encoding='utf-8'):
    session = None
    msg = MIMEText(body, 'plain', encoding)
    msg['Subject'] = Header(subject, encoding)
    msg['From'] = Header(SENDER_NAME, encoding)
    msg['To'] = recipient
    msg['Date'] = formatdate()
    try:
        session = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
        if SMTP_TLS:
            session.ehlo()
            session.starttls()
            session.ehlo()
            session.login(MAIL_ACCOUNT, MAIL_PASSWORD)
        session.sendmail(MAIL_ACCOUNT, recipient, msg.as_string())
    except Exception as e:
        raise e
    finally:                                                                                                                                                      11,1          Top
